"use strict";

const path = require("path");

const errorCodes = require("./error-codes");

const config = {
  application: {
    errorCodes,
    collections: {
      FILE: 'files'
    },
  },
  plugins: {
    appRestfront: {
      contextPath: "/api",
      mappingStore: {
        intro: path.join(__dirname, "../lib/mappings/restfront"),
      },
    },
    appDatastore: {
      mappingStore: {
        "intro-datastore": path.join(__dirname, "../lib/mappings/datastore"),
      },
    },
    appFilestore: {
      contextPath: "/filestore",
      uploadDir: path.join(__dirname, '../data'),
      collections: {
        FILE: 'files'
      },
    },
    appTracelog: {
      tracingPaths: [ '/filestore' ],
      tracingBoundaryEnabled: true,
    },
    appWebweaver: {
      cors: {
        enabled: true,
        mode: "simple",
      },
    },
    appWebserver: {
      host: "0.0.0.0",
      port: 7979,
      verbose: true,
    },
  },
  bridges: {
    mongoose: {
      appDatastore: {
        manipulator: {
          connection_options: {
            host: "127.0.0.1",
            port: "27017",
            name: "filestore",
            host: process.env["appDatastore_mongoose_manipulator_host"] || "127.0.0.1",
            port: process.env["appDatastore_mongoose_manipulator_port"] || "27017",
            name: process.env["appDatastore_mongoose_manipulator_db"] || "filestore",
          },
        },
      },
    }
  },
};

module.exports = config;
